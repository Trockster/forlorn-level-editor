﻿using UnityEngine;
using System.Collections;

public class Buschscript : MonoBehaviour {

	public Animator animator;
	public float idleAnim1;
	public float idleAnim2;

	public float animSwitch = 0;
	// Use this for initialization
	void Start () {
		idleAnim1 = Random.Range (0f, 2f);
		idleAnim2 = Random.Range (0f, 2f);

	}
	
	// Update is called once per frame
	void Update () {
		animSwitch += Time.deltaTime;

		if (idleAnim1 >= 1 && idleAnim2 >= 1) 
		{
			animator.SetBool ("Idle1", true);
		}
		if (idleAnim1 <= 1 && idleAnim2 <= 1) 
		{
			animator.SetBool ("Idle2", true);
		}
		if (idleAnim1 >= 1 && idleAnim2 <= 1) 
		{
			animator.SetBool ("Idle3", true);
		}
		if (idleAnim1 <= 1 && idleAnim2 >= 1) 
		{
			animator.SetBool ("Idle4", true);
		}
		if (animSwitch >= 3)
		{
			SwitchAnimation();
			animSwitch = 0;
		}

	}
	void SwitchAnimation()
	{
			idleAnim1 = Random.Range (0f, 2f);
			idleAnim2 = Random.Range (0f, 2f);
	}
}
