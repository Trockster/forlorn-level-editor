﻿using UnityEngine;
using System.Collections;

public class WormHurt : MonoBehaviour {

	public Animator animator;
	public BossController bosscontroller;



	private bool hit = false;
	private bool part2 = false;

	private float waitCount = 0;
	private float maxWait = 2.3f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (animator.GetCurrentAnimatorStateInfo (0).IsName ("Boss_Hurt_Idle1") || animator.GetCurrentAnimatorStateInfo (0).IsName ("Boss_Hurt_Idle2")) 
		{
			waitCount += Time.deltaTime;
		}
		if (waitCount >= maxWait) {
			waitCount = 0;
			if (part2 == false) {
				animator.SetTrigger ("Run1");
			} else {
				animator.SetTrigger ("Run2");
			}
		}
	
	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.name == "AttackTrigger") {

			if (part2 == false) {
				if (hit == false) {
					animator.SetTrigger ("Hit1");
					hit = true;
					waitCount = 0;
				} else {
					animator.SetTrigger ("Hit2");
					hit = false;
					part2 = true;
					bosscontroller.SendMessage ("Phase2Start");
					waitCount = 0;
				}
			} 
		}
		if (other.gameObject.tag == "ArkanBullet") {
			if (part2 == true) {
				if (hit == false) {
					animator.SetTrigger ("Hit11");
					hit = true;
					waitCount = 0;
				} else {
					animator.SetTrigger ("Hit22");
					hit = false;
					waitCount = 0;
					bosscontroller.SendMessage ("SpawnNow", 2f);
				}
			}	

		}
	}

}
