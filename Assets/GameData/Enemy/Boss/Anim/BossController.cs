﻿using UnityEngine;
using System.Collections;

public class BossController : MonoBehaviour {

	public Animator animator;

	public bool Phase1 = false;
	public bool Phase2 = false;
	public bool Phase3 = false;
	public bool Phase4 = false;
	public bool Phase5 = false;

	public GameObject MemPrefab;
	public GameObject BossBlock;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void BeginNow()
	{
		
		Invoke ("Phase1Start", 2f);
	}

	void Phase1Start ()
	{
		animator.SetTrigger ("Attack1");
	}
	void Phase2Start()
	{
		animator.SetTrigger ("Attack2");
	}
	void SpawnNow()
	{
		Invoke ("Spawn", 5f);
	}
	void Spawn()
	{
		Instantiate (MemPrefab, transform.position, Quaternion.identity);
		Instantiate (BossBlock, transform.position, Quaternion.identity);
	}













	void ShakeStart()
	{
		GetComponent<BossShake> ().enabled = true;
	}
	void ShakeStop()
	{
		GetComponent<BossShake> ().enabled = false;
	}
}
