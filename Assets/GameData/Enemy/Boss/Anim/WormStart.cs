﻿using UnityEngine;
using System.Collections;

public class WormStart : MonoBehaviour {

	public Animator animator;
	public BossController bosscontroller;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter2D (Collider2D collide)
	{
		if (collide.gameObject.tag == "Player") 
		{
			animator.SetTrigger("Start");
			bosscontroller.SendMessage("BeginNow");
		}
	}
}
