﻿using UnityEngine;
using System.Collections;

public class BirdScript : MonoBehaviour {

	public Animator animator;
	public bool idle = true;
	public bool count = false;
	public float counter = 0; 
	public float countMax =  30f; 

	private float animCounter = 0;
	private float animCountMax = 1.49f;
	public float idleAnim;

	// Use this for initialization
	void Start () {
		idleAnim = Random.Range (0f, 2f);
	}
	
	// Update is called once per frame
	void Update () {

		if (idle != true && counter >= countMax)
		{
			animator.SetTrigger ("land");
			count = false;
			counter = 0;
		}
		if (idle == true)
		{
			if (idleAnim >= 1) {
				animator.SetBool ("Idle1", true);
			} else {
				animator.SetBool ("Idle2", true);
			}
		}
		if (count == true) 
		{
			counter += Time.deltaTime * 10f;
		}
		CountAnim ();
	}
	void CountAnim()
	{
		animCounter += Time.deltaTime ;
		if (animCounter >= animCountMax) 
		{
			idleAnim = Random.Range (0f, 2f);
			animCounter = 0;
		}
	}

	void OnTriggerEnter2D( Collider2D other)
	{
		if (other.gameObject.tag == "Player") {
			Debug.Log ("shite");
			animator.SetTrigger ("Fly");
			idle = false;

		}
	}
	void OnTriggerExit2D( Collider2D other)
	{
		if (other.gameObject.tag == "Player") {
			count = true;
			
		}
	}

}
