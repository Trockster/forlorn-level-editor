﻿using UnityEngine;
using System.Collections;

public class GateScript : MonoBehaviour {

	public Animator animator;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	

	}
	public void OpenGate ()
	{
		animator.SetTrigger ("GateOpen");
	}
	public void CloseGate()
	{
		animator.SetTrigger ("GateClose");
	}
}
