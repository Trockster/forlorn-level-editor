﻿using UnityEngine;
using System.Collections;

public class MementoShoot : MonoBehaviour {

	private GameObject myTarget;
	private Vector2 targetPos;
	public float speed = 100;

	public bool floating = false;
	public bool returning = false;


	public float LightPressure;
	public float lightMax = 8f;



	public Rigidbody2D Memes;


	// Use this for initialization
	void Start () {
		Memes.gravityScale = 2;
		myTarget = GameObject.FindGameObjectWithTag ("Player");
		targetPos = myTarget.transform.position;
		Physics2D.IgnoreLayerCollision (10, 11, true);


		Vector2 right = new Vector2 (Random.Range(-15,15),Random.Range(15,-15));
			Memes.AddForce (right, ForceMode2D.Impulse);
			

	}
	
	// Update is called once per frame
	void Update () {
		targetPos = myTarget.transform.position;
		LightPressure += Time.deltaTime;
		if (LightPressure >= lightMax) 
		{
			returning = true;
			Memes.gravityScale = 0;
			Invoke ("Destruction", 1f);
		}

		if (returning == true) {
			transform.position = Vector2.MoveTowards (transform.position, targetPos, speed * Time.deltaTime);
			Physics2D.IgnoreLayerCollision (10, 11, false);
		}
	}
	void Destruction()
	{
		Destroy (this.gameObject);
	}


}
