﻿using UnityEngine;
using System.Collections;

public class Boxbox : MonoBehaviour {

	public bool atBox;
	public float playerNormalSpeed = 80f;
	private Rigidbody2D rbBox;
	private PlayerController playerSC;
	public Rigidbody2D rbPlayer;



	public bool dontJump = false;
	//grounded
	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask groundlayer;
	public bool groundedbox;

	void Start() {

		 
		//zugrif auf den player speed auf dem Object player.2 3d im script Playercontroller
		playerSC = GameObject.Find ("Player.2 3D").GetComponent<PlayerController> ();
		rbBox = GetComponent<Rigidbody2D> ();



	}
	void Update(){

		//groundcheck
		groundedbox = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, groundlayer);


			//bewegung der Kiste
		if (Input.GetKey (KeyCode.Y) && atBox && groundedbox && GameObject.Find ("Player.2 3D").GetComponent<PlayerController> ().grounded && GameObject.Find ("Player.2 3D").gameObject.GetComponent<PlayerController> ().Char1 || Input.GetButton ("X_Button") && atBox && groundedbox && GameObject.Find ("Player.2 3D").GetComponent<PlayerController> ().grounded && GameObject.Find ("Player.2 3D").gameObject.GetComponent<PlayerController> ().Char1) {


		
				if (Input.GetKeyDown (KeyCode.Y) || Input.GetButtonDown ("X_Button")) {

					//kniematic wird aus angemacht
					rbBox.isKinematic = false;
					dontJump = true;
					playerSC.c_maxSpeed = 40f;
					GameObject.Find ("Boxdont").GetComponent<BoxCollider2D> ().isTrigger = false;

					//erstellen eines FixedJoints
					gameObject.AddComponent<FixedJoint2D> ();

					//FixedJoint Zuweisen
					gameObject.GetComponent<FixedJoint2D> ().connectedBody = rbPlayer;
					GetComponent<FixedJoint2D> ().enableCollision = true;
				} 

			} else {
				if (groundedbox == true) {

					//kniematic wird aus angemacht
					rbBox.isKinematic = true;
					GameObject.Find ("Boxdont").GetComponent<BoxCollider2D> ().isTrigger = true;
					//joint wird zerstört
					Destroy (gameObject.GetComponent ("FixedJoint2D"));



				}
			}

		if (Input.GetKeyUp(KeyCode.Y) || Input.GetButtonUp ("X_Button") ) {
			
			//normal speed wird nach loslassen der Y zurückgegeben
			//playerSC.c_maxSpeed = playerNormalSpeed;
			dontJump = false;
		}
	}

	void FixedUpdate () {
		//geguckt ob die kist am ground ist
		if (groundedbox == false) {
			rbBox.isKinematic = false;

			
		


		}



			

		}
	

	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D collide){

		if (collide.gameObject.tag == "PlayerBoxSchieben") {

			atBox = true;

		}
	}
	void OnTriggerExit2D(Collider2D collide) {
		if (collide.gameObject.tag == "PlayerBoxSchieben") {
			playerSC.c_maxSpeed = playerNormalSpeed;
			atBox = false;

		}
	}
}
