﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Szene1Sc : MonoBehaviour {

	public float waitTime = 2f;
	private float wait = 0;


	// Use this for initialization
	void Start () {
	

	}
	
	// Update is called once per frame
	void Update () {
		wait += Time.deltaTime;

		if (wait >= waitTime) 
		{
			Invoke ("BeginNow", 10f);
		}
	}
	IEnumerator ChangeLevel()
	{

		float fadeTime = GameObject.Find ("FadeManager").GetComponent<Fading> ().BeginFade (1);
		yield return new WaitForSeconds (fadeTime);
		SceneManager.LoadScene ("Artsyfartsy");


	}

	void BeginNow () {

		StartCoroutine("ChangeLevel");
	}
}
