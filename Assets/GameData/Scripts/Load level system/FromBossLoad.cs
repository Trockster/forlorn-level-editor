﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FromBossLoad : MonoBehaviour {

	public float waitTime = 3.5f;
	private float wait = 0;


	// Use this for initialization
	void Start () {
	

	}
	
	// Update is called once per frame
	void Update () {
		wait += Time.deltaTime;

		if (wait >= waitTime) 
		{
			Invoke ("BeginNow", 10f);
		}
	}
	IEnumerator ChangeLevel()
	{

		float fadeTime = GameObject.Find ("FadeManager").GetComponent<Fading> ().BeginFade (1);
		yield return new WaitForSeconds (fadeTime);
		SceneManager.LoadScene ("Menu");


	}

	void BeginNow () {

		StartCoroutine("ChangeLevel");
	}
}
