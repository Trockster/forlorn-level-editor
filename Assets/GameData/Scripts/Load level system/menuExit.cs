﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class menuExit : MonoBehaviour {

	public Button ButtonExit;
	void Start () {
		Button btn = ButtonExit.GetComponent<Button> ();
		btn.onClick.AddListener (TaskOnClick);

	}
	// Use this for initialization
	IEnumerator ChangeLevel()
	{

		float fadeTime = GameObject.Find ("FadeManager").GetComponent<Fading> ().BeginFade (1);
		yield return new WaitForSeconds (fadeTime);
		Application.Quit ();

	}

	void TaskOnClick () {

		StartCoroutine("ChangeLevel");
	}
}
