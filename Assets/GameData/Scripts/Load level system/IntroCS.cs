﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IntroCS : MonoBehaviour {

	public float waitTime = 1f;
	private float wait = 0;


	// Use this for initialization
	void Start () {
	

	}
	
	// Update is called once per frame
	void Update () {
		wait += Time.deltaTime;

		if (wait >= waitTime) 
		{
			Invoke ("BeginNow", 1f);
		}
	}
	IEnumerator ChangeLevel()
	{

		float fadeTime = GameObject.Find ("FadeManager").GetComponent<Fading> ().BeginFade (1);
		yield return new WaitForSeconds (fadeTime);
		SceneManager.LoadScene ("Menu");


	}

	void BeginNow () {

		StartCoroutine("ChangeLevel");
	}
}
