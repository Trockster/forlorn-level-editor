﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class LoadBoss : MonoBehaviour {

	public bool nextScene = false;


	IEnumerator ChangeLevel()
	{

		float fadeTime = GameObject.Find ("FadeManager").GetComponent<Fading> ().BeginFade (1);
		yield return new WaitForSeconds (fadeTime);
		SceneManager.LoadScene ("TestSceneE");


	}
	void OnTriggerEnter2D (Collider2D collid)

	{

		if (collid.gameObject.CompareTag("Player")){



			StartCoroutine("ChangeLevel");

		}

	}
}