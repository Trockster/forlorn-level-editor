﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class backmenu : MonoBehaviour {



	public Button Buttonback;
	void Start () {
		Button btn = Buttonback.GetComponent<Button> ();
		btn.onClick.AddListener (TaskOnClick);

	}
	// Use this for initialization
	IEnumerator ChangeLevel()
	{

		float fadeTime = GameObject.Find ("FadeManager").GetComponent<Fading> ().BeginFade (1);
		yield return new WaitForSeconds (fadeTime);
		SceneManager.LoadScene ("Menu");


	}

	void TaskOnClick () {

		StartCoroutine("ChangeLevel");
	}
}
