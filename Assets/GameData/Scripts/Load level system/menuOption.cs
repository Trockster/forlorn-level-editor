﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class menuOption : MonoBehaviour {


	
	public Button ButtonOption;
	void Start () {
		Button btn = ButtonOption.GetComponent<Button> ();
		btn.onClick.AddListener (TaskOnClick);

	}
	// Use this for initialization
	IEnumerator ChangeLevel()
	{

		float fadeTime = GameObject.Find ("FadeManager").GetComponent<Fading> ().BeginFade (1);
		yield return new WaitForSeconds (fadeTime);
		SceneManager.LoadScene ("Option");


	}

	void TaskOnClick () {

		StartCoroutine("ChangeLevel");
	}
}
