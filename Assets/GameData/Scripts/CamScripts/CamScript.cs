﻿using UnityEngine;
using System.Collections;

public class CamScript : MonoBehaviour {


	[SerializeField]protected Transform trackingTarget;

	[SerializeField]float xOffset = 5;

	public float yOffset = 7;

	[SerializeField]protected float followSpeed;


	public bool isXLocked = false;


	public bool isYLocked = false;

	public Transform TrackingTarget
	{
		get
		{
			return trackingTarget;
		}
		set
		{
			trackingTarget = value;
		}
	}
	void FixedUpdate()
	{
		
		float xTarget = trackingTarget.position.x + xOffset;
		float yTarget = trackingTarget.position.y + yOffset;

		float xNew = transform.position.x;
		if (!isXLocked)
		{
			xNew = Mathf.Lerp(transform.position.x, xTarget, Time.deltaTime * followSpeed);
		}

		float yNew = transform.position.y;
		if (!isYLocked)
		{
			yNew = Mathf.Lerp(transform.position.y, yTarget, Time.deltaTime * followSpeed);
		}
		transform.position = new Vector3(xNew, yNew, transform.position.z);

	}

}
