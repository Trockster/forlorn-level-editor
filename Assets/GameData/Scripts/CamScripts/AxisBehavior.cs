﻿using UnityEngine;
using System.Collections;


//This Script is placed on the player

public class AxisBehavior : MonoBehaviour
{
	#region Public Fields

	[SerializeField]
	Camera camera;

	[SerializeField]
	string camstop;

	[SerializeField]
	string GroundCamStop;
	#endregion

	#region Private

	private Transform previousTarget;

	private CamScript AxisLock;

	private bool onLadder = false;

	#endregion					

	// Use this for initialization
	void Start()
	{
		AxisLock = camera.GetComponent<CamScript>();
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == camstop && !onLadder)
		{
			onLadder = true;
			PushTarget();
		}
		if (other.tag == GroundCamStop)
		{
			Invoke ("ALock", 1f);
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == camstop && onLadder)
		{
			onLadder = false;
			PopTarget();
		}
	}


	private void PushTarget()
	{
		AxisLock.isYLocked = false;
		AxisLock.isXLocked = true;
		AxisLock.yOffset = 4;
		Debug.Log (111);
	}

	private void PopTarget()
	{
		AxisLock.isXLocked = false;
		AxisLock.yOffset = 7;
	}
	private void ALock()
	{
		if (!onLadder) 
		{
			AxisLock.isYLocked = true;
		}
	}
}