﻿using UnityEngine;
using System.Collections;


//This Script is placed on the player

public class LockBehavior : MonoBehaviour
{
	#region Public Fields

	[SerializeField]
	Camera camera;

	[SerializeField]
	string tag;

	#endregion

	#region Private

	private Transform previousTarget;

	private CamScript trackingBehavior;

	private bool isLocked = false;

	#endregion

	// Use this for initialization
	void Start()
	{
		trackingBehavior = camera.GetComponent<CamScript>();
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == tag && !isLocked)
		{
			isLocked = true;
			PushTarget(other.transform);
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == tag && isLocked)
		{
			isLocked = false;
			PopTarget();
		}
	}

	private void PushTarget(Transform newTarget)
	{
		previousTarget = trackingBehavior.TrackingTarget;
		trackingBehavior.TrackingTarget = newTarget;
	}

	private void PopTarget()
	{
		trackingBehavior.TrackingTarget = previousTarget;
	}

}