﻿using UnityEngine;
using System.Collections;

public class LightShoot : MonoBehaviour {

	private GameObject myTarget;
	private Vector2 targetPos;
	public float speed = 200;

	public bool floating = false;
	public bool returning = false;


	public float LightPressure;
	public float lightMax = 0.5f;



	public Rigidbody2D Light;


	// Use this for initialization
	void Start () {
		Light.gravityScale = 5;
		myTarget = GameObject.FindGameObjectWithTag ("Player");
		targetPos = myTarget.transform.position;
		Physics2D.IgnoreLayerCollision (10, 11, true);

		if (GameObject.Find("Player.2 3D").GetComponent<PlayerController>().dirleft) 
			{
			Vector2 right = new Vector2 (-30, 31);
			Light.AddForce (right, ForceMode2D.Impulse);
			}
		else
			{
			Vector2 left = new Vector2 (30, 31);
			Light.AddForce (left, ForceMode2D.Impulse);
			}

	}
	
	// Update is called once per frame
	void Update () {
		targetPos = myTarget.transform.position;
		if (floating == true)
		{
			LightPressure += Time.deltaTime *10f;
			if (LightPressure >= lightMax) 
			{
				Light.AddForce (transform.up * 41);
				LightPressure = 0;
				Invoke("startReturn", 5f);
			}
		}
		if (returning == true) {
			transform.position = Vector2.MoveTowards (transform.position, targetPos, speed * Time.deltaTime);
			Physics2D.IgnoreLayerCollision (10, 11, false);
		}
		if (Input.GetButtonDown ("B_Button") && floating == true || Input.GetKeyDown (KeyCode.E) && floating == true) 
		{
			Physics2D.IgnoreLayerCollision (10, 11, false);
			startReturn ();
		}
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		
		Light.AddForce (transform.up * 130);
		Invoke("floatingStart", 0.25f);
		if (returning == true) {
			if (coll.gameObject.tag == "Player") {
				Physics2D.IgnoreLayerCollision (10, 8, false);
				Physics2D.IgnoreLayerCollision (10, 9, false);
				Light.gravityScale = 5;
				Destroy (this.gameObject);
			}
		}


	}
		




	void floatingStart()
	{
		floating = true ;
		Physics2D.IgnoreLayerCollision (10, 11, true);
	}
	void floatingStop()
	{
		floating = false ;
		Physics2D.IgnoreLayerCollision (10, 11, true);
	}
	void startReturn()
	{
		Physics2D.IgnoreLayerCollision (10, 8, true);
		Physics2D.IgnoreLayerCollision (10, 9, true);
		Physics2D.IgnoreLayerCollision (10, 11, false);
		Light.gravityScale = 0;
		
		floating = false;
		returning = true;


	}

}
