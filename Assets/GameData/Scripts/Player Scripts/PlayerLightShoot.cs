﻿using UnityEngine;
using System.Collections;

public class PlayerLightShoot: MonoBehaviour {

	//ShootKram
	public GameObject lightPrefab;
	public GameObject bulletSpawn;

	//Shoot Kram Cool down
	public bool flyingLight = false;
	public float lightTimer;
	public float lightCoolDown = 3f;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (gameObject.GetComponent<PlayerController> ().Char2) 
		{
			if (Input.GetButtonDown ("B_Button") && !flyingLight || Input.GetKeyDown (KeyCode.E) && !flyingLight) {
				flyingLight = true;

				lightTimer = lightCoolDown;
				GameObject bullet = (GameObject)Instantiate (lightPrefab, bulletSpawn.transform.position, Quaternion.identity);



			}
			if (flyingLight) {
				if (lightTimer > 0) {
					lightTimer -= Time.deltaTime;
				} else {
					flyingLight = false;

				}




			}
		}

	}
}