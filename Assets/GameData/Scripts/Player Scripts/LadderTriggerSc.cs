﻿using UnityEngine;
using System.Collections;

public class LadderTriggerSc : MonoBehaviour {

	public PlayerClimb player;

	[SerializeField]
	string Wall;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == Wall) {
			
			player.stickLadder ();
		}
	}
	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == Wall) {

			player.unStickLadder ();
		}
	}
}
