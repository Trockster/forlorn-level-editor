﻿using UnityEngine;
using System.Collections;

public class AbilityController : MonoBehaviour {

	public bool ePunch = false;
	public bool eDash = false;
	public bool eShoot = false;
	public bool eLight = false;

	public GameObject MemesPrefab;

	public Animator animator;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (ePunch == true) 
		{
			if (!GetComponent<PlayerAttack> ().enabled) {
				GetComponent<PlayerAttack> ().enabled = true;
			}
		}
		if (eDash == true) 
		{
			if (!GetComponent<Dashy> ().enabled) {
				GetComponent<Dashy> ().enabled = true;
			}
		}
		if (eShoot == true) 
		{
			if (!GetComponent<PlayerShoot> ().enabled) {
				GetComponent<PlayerShoot> ().enabled = true;
			}
		}
		if (eLight == true) 
		{
			if (GetComponent<PlayerLightShoot> ().enabled) {
				GetComponent<PlayerLightShoot> ().enabled = true;
			}
		}
	}
	void OnTriggerEnter2D (Collider2D collide)
	{
		
		if (collide.gameObject.name == "PunchMemento") 
		{
			GetComponent<PlayerAttack> ().enabled = true;
			ePunch = true;
			Destroy(collide.gameObject);
			Instantiate (MemesPrefab,transform.position, Quaternion.identity);
			animator.SetTrigger ("Sound");

		}
		if (collide.gameObject.name == "DashMemento") 
		{
			GetComponent<Dashy> ().enabled = true;
			eDash = true;
			Destroy(collide.gameObject);
			Instantiate (MemesPrefab,transform.position, Quaternion.identity);
			animator.SetTrigger ("Sound");
		}
		if (collide.gameObject.name == "ShootMemento") 
		{
			GetComponent<PlayerShoot> ().enabled = true;
			eDash = true;
			Destroy(collide.gameObject);
			Instantiate (MemesPrefab,transform.position, Quaternion.identity);
			animator.SetTrigger ("Sound");
		}
		if (collide.gameObject.name == "LightMemento") 
		{
			GetComponent<PlayerLightShoot> ().enabled = true;
			eLight = true;
			Destroy(collide.gameObject);
			Instantiate (MemesPrefab,transform.position, Quaternion.identity);
			animator.SetTrigger ("Sound");
		}
		if (collide.gameObject.tag == "Memento") 
		{
			Destroy(collide.gameObject);
			Instantiate (MemesPrefab,transform.position, Quaternion.identity);
			animator.SetTrigger ("Sound");
		}
	}
}
