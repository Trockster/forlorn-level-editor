﻿using UnityEngine;
using System.Collections;

public class PlayerClimb : MonoBehaviour {

	public Animator animator;
	public Rigidbody2D player;
	public PlayerController playCon;

	public GameObject currentLadder;

	[SerializeField]
	string tag;

	private Transform ladder;
	public bool onLadder = false;
	public bool stick = false;

	[SerializeField] float c_jumpSpeed =70;
	[SerializeField] float c_jumpDist =11;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		//bestimmt die ausrichtung des spielers
		if (Input.GetAxis ("Horizontal") < 0 ) {
			transform.rotation = Quaternion.Euler (0, 180, 0);	
		}
		if (Input.GetAxis ("Horizontal") > 0 ) {
			transform.rotation = Quaternion.Euler (0, 0, 0);	
		}


		//lässt den spieler von der leiter springen
		if (Input.GetKeyDown (KeyCode.Space) && stick && Input.GetAxis ("Horizontal") != 0 || Input.GetButtonDown ("A_Button") && stick) 
		{
			player.isKinematic = false;
			if (Input.GetAxis ("Horizontal") == 0) {
				player.velocity = new Vector2 (0, c_jumpSpeed);
			} else {
				if (!gameObject.GetComponent<PlayerController> ().dirleft) {
					player.velocity = new Vector2 (c_jumpDist, c_jumpSpeed);

				} else {
					player.velocity = new Vector2 (-c_jumpDist, c_jumpSpeed);

				}

				//alle Animationen und dafür benötigten werte werden ein gestellt

				if (gameObject.GetComponent<PlayerController> ().Char1) {
					animator.SetTrigger ("Tl_Jump");
					animator.SetBool ("Sl_Running", false);
					animator.SetBool ("Sl_Idle", false);

				}
			}

			stick = false;
		}

		if (Input.GetAxis ("Vertical") > 0 && stick) 
		{
			player.AddForce (Vector2.up * 15);
			player.isKinematic = false;

		}
		if (Input.GetAxis ("Vertical") < 0 && stick) 
		{
			player.AddForce (Vector2.down * 15);
			player.isKinematic = false;

		}
		if (Input.GetAxis ("Vertical") == 0 && stick) {
			player.isKinematic = true;
		}


		if (stick == true) {
			
			player.gravityScale = 0;
			GetComponent<PlayerController> ().enabled = false;
			GetComponent<Dashy> ().enabled = false;
			GetComponent<PlayerAttack> ().enabled = false;


		} else {

			if (!gameObject.GetComponent<Dashy> ().dashing) 
			{
				player.gravityScale = 15;
			}
			GetComponent<PlayerController> ().enabled = true;
			GetComponent<Dashy> ().enabled = true;
			GetComponent<PlayerAttack> ().enabled = true;

		}

	}



	public void stickLadder()
	{
		onLadder = true;
		if (Input.GetAxis ("Vertical") > 0 && onLadder && !stick) 
		{

			stick = true;
			player.isKinematic = true;
		}

	}
	public void unStickLadder()
	{
		onLadder = false;
		stick = false;
	}
}
