﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public Animator animator;
	public GameObject Loader;





	public bool Char1 = true;
	public bool Char2 = false;



	//	[SerializeField] float c1_startSpeed = 5f;
	public float c_maxSpeed = 80;

	[SerializeField] float climbSpeed =5;
	[SerializeField] float holdSpeed =2;

	public GameObject boxHitPrefab;

	//erkennung welche in Richtig der Player guckt
	public bool dirleft;
	public Rigidbody2D player;

	// für die ground detection 
	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask groundlayer;
	public bool grounded;
	public bool groundedTrigger = false;

	//schalter Kram
	public bool amSchalter = false;



	// Use this for initialization
	void Start () {
		player = GetComponent<Rigidbody2D> ();
		//GetComponent<PlayerAttack> ().enabled = false;
		//GetComponent<Dashy> ().enabled = false;
		//GetComponent<PlayerShoot> ().enabled = false;
		//GetComponent<PlayerLightShoot> ().enabled = false;


	}
	// Update is called once per frame
	void FixedUpdate () {
		
		if (Input.GetKeyDown (KeyCode.W)||Input.GetButtonDown("Bumper_L")) 
		{
			if( !gameObject.GetComponent<Dashy>().switchLock  )
			{
			 if (Char1 == true) {
				Char1 = false;
				Char2 = true;
				animator.SetBool ("Sl_Running", false);
			 	animator.SetTrigger ("Tl_Morph");
			 } else {
				Char1 = true;
				Char2 = false;
				animator.SetBool ("Sh_Running", false);
				animator.SetTrigger ("Sh_Morph");
			 }
			}

		}

		if (Char1 == true)
		{
			update_char1 ();

		}
		if (Char2 == true)
		{		
			update_char2 ();

		}


	}
	// Update is called once per frame
	void Update () {	


		//wie der ground gefunden wird und jedes boden stück brauch den layer ground	

			grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, groundlayer);
	

		setDirection ();
	}

	//Both Characters have seperate update Methods
	void update_char1() {
		
		if (Input.GetAxis("Horizontal") != 0)
		{
			if (grounded == true) 
			{
				animator.SetBool ("Sl_Running", true);
				animator.SetBool ("Sl_Idle", false);
			}

			if (Input.GetAxis ("Horizontal") < 0) 
			{
				//player.velocity = new Vector2 (-maxSpeed, 0);
				transform.rotation = Quaternion.Euler (0, 180, 0);	

					player.AddForce (Vector2.left * c_maxSpeed);

					
			}

			if (Input.GetAxis ("Horizontal") > 0) 
			{
				//player.velocity = new Vector2 (maxSpeed , 0);
				transform.rotation = Quaternion.Euler (0, 0, 0);

					player.AddForce (Vector2.right * c_maxSpeed);


			}
		}
		else 
		{
			animator.SetBool ("Sl_Running", false);
			animator.SetBool ("Sl_Idle", true);
		}
			
	}


	void update_char2() {
		if (Input.GetAxis("Horizontal") != 0){
			if (grounded == true) {
				animator.SetBool ("Sh_Running", true);
				animator.SetBool ("Sh_Idle", false);
			}
			if (Input.GetAxis ("Horizontal") < 0) {
				//player.velocity = new Vector2 (-maxSpeed, 0);
				player.AddForce (Vector2.left * c_maxSpeed);
				transform.rotation = Quaternion.Euler (0, 180, 0);		
			}

			if (Input.GetAxis ("Horizontal") > 0) {
				//player.velocity = new Vector2 (maxSpeed , 0);
				player.AddForce (Vector2.right * c_maxSpeed);
				transform.rotation = Quaternion.Euler (0, 0, 0);
			}
		} else {
			animator.SetBool ("Sh_Running", false);
			animator.SetBool ("Sh_Idle", true);
		}

	}

	void speedControll()
	{
		if (grounded == false) {
			c_maxSpeed = 50;
		} else {
			c_maxSpeed = 80;
		}
	}
	void setDirection()
	{
		if (Input.GetAxis ("Horizontal") < 0) 
		{
			dirleft = true;
		}
		if (Input.GetAxis ("Horizontal") > 0) 
		{
			dirleft = false;
		}


	}

	void OnTriggerEnter2D (Collider2D collide)
	{
		//Das wird nicht umbedingt benötigt. Lass es aber erstmal drin. Ist eine Abfrage des Schalters	 
		//Ist der Player am Schalter == ja
		if (collide.gameObject.tag == "Schalter") {
			amSchalter = true;

		}


			
	}


	//Das wird nicht umbedingt benötigt. Lass es aber erstmal drin. Ist eine Abfrage des Schalters
	void OnTriggerExit2D(Collider2D collide) {
		//ist der Player am Schalter == Nein
		if (collide.gameObject.tag == "Schalter") {
			amSchalter = false;

		}


	}



}




