﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class PlayerHitbox : MonoBehaviour {

	public LevelManager levelManager;
	public Animator animator;
	public Animator parAnim;

	[SerializeField]bool Hit_1;

	public float HealCoolDown = 3f;
	public float HealCharge = 0;





	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Hit_1 == true)
		{
			Heal ();
		}
	}
	void OnTriggerEnter2D (Collider2D collide)
	{

		if (collide.gameObject.tag == "Thorne" && !transform.parent.gameObject.GetComponent<PlayerAttack> ().attacking) {

			parAnim.SetTrigger ("HitMe");

			StartCoroutine("RespawnFadeIn");


		}
		
		if (collide.gameObject.tag == "Enemy" && !transform.parent.gameObject.GetComponent<PlayerAttack> ().attacking) {

			parAnim.SetTrigger ("HitMe");

			HitByEnemy();
		}
		if (collide.gameObject.tag == "EnemyBullet" && !transform.parent.gameObject.GetComponent<PlayerAttack> ().attacking) {
			Destroy (collide.gameObject);
			parAnim.SetTrigger ("HitMe");

			HitByEnemy();
		}
		if (collide.gameObject.tag == "Boss" && !transform.parent.gameObject.GetComponent<PlayerAttack> ().attacking) {
			//Destroy (this.gameObject);
			parAnim.SetTrigger ("HitMe");

			HitByEnemy();
			StartCoroutine("RespawnFadeboss");

		}
	}
	void HitByEnemy()
	{		
		if (!Hit_1) {
			Hit_1 = true;
		} else {
			StartCoroutine("RespawnFadeIn");

		}

	}
	void Heal()
	{
		HealCharge += Time.deltaTime;
		if (HealCharge >= HealCoolDown)
		{
			Hit_1 = false;
			HealCharge = 0;
		}
	}
	IEnumerator RespawnFadeIn()
	{

		float fadeTime = GameObject.Find ("FadeManager").GetComponent<Deathfade> ().BeginFade (1);
		yield return new WaitForSeconds (fadeTime);
		Debug.Log ("as");
		Respawnplayer ();


	}

	IEnumerator RespawnFadeOut()
	{

		float fadeTime = GameObject.Find ("FadeManager").GetComponent<Deathfade> ().BeginFade (-1);
		yield return new WaitForSeconds (fadeTime);
		Debug.Log ("was");


	}
	IEnumerator RespawnFadeboss()
	{

		float fadeTime = GameObject.Find ("FadeManager").GetComponent<Deathfade> ().BeginFade (1);
		yield return new WaitForSeconds (fadeTime);
		Debug.Log ("as");
		SceneManager.LoadScene ("TestSceneE");


	}

	void Respawnplayer (){
		StopCoroutine("RespawnFadeIn");
		Debug.Log ("aut");
		StartCoroutine("RespawnFadeOut");
		levelManager.RespawnPlayer ();
	}
}
