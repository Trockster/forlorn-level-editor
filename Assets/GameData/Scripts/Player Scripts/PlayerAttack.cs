﻿using UnityEngine;
using System.Collections;

public class PlayerAttack : MonoBehaviour {

	public Animator animator;

	public bool attacking = false;

	private float attackTimer = 0;
	private float attackCd = 0.3f;

	public Collider2D attackTrigger;

	private Animator anim;




	void Start() {

	}

	// Use this for initialization
	void Awake () {
	
		anim = gameObject.GetComponent<Animator> ();
		attackTrigger.enabled = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (gameObject.GetComponent<PlayerController> ().Char1)      //zugreifen auf dem Bool amSchalter der Im Schalter Script ist auf dem Schalter																																														//Ist der Zugriff auf das bool im Playercontroll. Wird momentan nicht benutzt
		{																													
			if (Input.GetButtonDown ("X_Button") && !attacking &&   !gameObject.GetComponent<PlayerController>().amSchalter  && !GameObject.FindWithTag("Box").GetComponent<Boxbox>().atBox|| Input.GetKeyDown (KeyCode.Y) && !attacking && !gameObject.GetComponent<PlayerController>().amSchalter && !GameObject.FindWithTag("Box").GetComponent<Boxbox>().atBox) { /* !gameObject.GetComponent<PlayerController>().amSchalter*/ 
				attacking = true;
				attackTimer = attackCd;

				attackTrigger.enabled = true;

				animator.SetTrigger ("Tl_Punch");
				GetComponent<CameraShake> ().enabled = true;
			}

			if (attacking) {
				if (attackTimer > 0) {
					attackTimer -= Time.deltaTime;
				} else {
					attacking = false;
					attackTrigger.enabled = false;
				}
			}
		}
	}
}
