﻿using UnityEngine;
using System.Collections;

public class Dashy : MonoBehaviour {

	public Animator animator;
	public GameObject Loader;
	public Rigidbody2D player;

	public float DashPressure;
	public float maxDashPressure = 4f;
	public float dashSpeed = 3000;
	public bool dash;
	public bool switchLock = false;

	public bool dashing = false;
	public bool dashed = false;
	[SerializeField] float dashCooldown = 10f;
	public float dashReload = 0;
	private bool blinked = false;
	private bool load = false;


	// Use this for initialization
	void Start () {
		player.gravityScale = 15;
	}
	
	// Update is called once per frame
	void Update () {
		if (gameObject.GetComponent<PlayerController> ().Char1 && dashed == false) {
			if (Input.GetKey (KeyCode.E) || Input.GetButton ("B_Button")) {

				switchLock = true;
				if (!load) 
				{
					Loader.SetActive (true);
					load = true;
				}
				if (DashPressure < maxDashPressure) {
					DashPressure += Time.deltaTime * 10f;
				} else {
					DashPressure = maxDashPressure;
				}
				player.isKinematic = true;

			} else {
				player.isKinematic = false;
				switchLock = false;
			}
			if (Input.GetKeyUp (KeyCode.E) || Input.GetButtonUp ("B_Button")) {
				
				Loader.SetActive (false);
				player.isKinematic = false;
				if (DashPressure == maxDashPressure) {

					dashing = true;
					dashed = true;
					player.gravityScale = 0;
					if (gameObject.GetComponent<PlayerController> ().dirleft) {


						player.AddForce (Vector2.left * dashSpeed);
						animator.SetTrigger ("Sl_Dashing");
						DashPressure = 0;

						Invoke ("gravityReset", 0.25f);

					}
					if (!gameObject.GetComponent<PlayerController> ().dirleft) {
						player.AddForce (transform.right * dashSpeed);
						animator.SetTrigger ("Sl_Dashing");
						DashPressure = 0;
						Invoke ("gravityReset", 0.25f);
					}
					GetComponent<DashShake> ().enabled = true;
				} else {
					DashPressure = 0;
					player.isKinematic = false;
				}
				load = false;
			}
		}
		if (DashPressure == maxDashPressure && !blinked) {
			animator.SetTrigger ("Blink");
			Loader.SetActive (false);
			blinked = true;

		}

		if (dashed == true) 
		{
			dashReload += Time.deltaTime * 10f;
			if (dashReload >= dashCooldown) 
			{
				dashed = false;
				blinked = false;
				dashReload = 0;
			}
		}
	}
	void LateUpdate()
	{
		if (!Input.GetKey (KeyCode.E) && !Input.GetButton ("B_Button")) {
			
			Loader.SetActive (false);
			DashPressure = 0;
		}
	}
	void gravityReset()
	{
		player.gravityScale = 15;
		dashing = false;
	}
}
