﻿using UnityEngine;
using System.Collections;

public class PlayerJump : MonoBehaviour {

	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask groundlayer;
	public bool grounded;
	public bool groundedTrigger;



	public Rigidbody2D player;
	public Animator animator;

	[SerializeField] float c_jumpSpeed =70;
	[SerializeField] float c_jumpDist =11;



	// Use this for initialization
	void Start () {
		groundedTrigger = false;

	}
	/*void LateUpdate () {
		Animate ();
	
	}*/
	
	// Update is called once per frame
	public virtual void LateUpdate () {
		Animate ();

		 grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, groundlayer);


		if (Input.GetKeyDown (KeyCode.Space) && grounded && !GameObject.FindWithTag("Box").GetComponent<Boxbox>().dontJump || Input.GetButtonDown("A_Button")&& grounded && !GameObject.FindWithTag("Box").GetComponent<Boxbox>().dontJump) {
			Invoke("unGround", 0.2f);


			//Es wird geguckt ob bewegt und welche ausrichtung um Sprungstärke zu bestimmen
			if (Input.GetAxis ("Horizontal") == 0 ) {
				player.velocity = new Vector2 (0, c_jumpSpeed);
			} else {
				if (!gameObject.GetComponent<PlayerController> ().dirleft) {
					player.velocity = new Vector2 (c_jumpDist, c_jumpSpeed);

				} else {
					player.velocity = new Vector2 (-c_jumpDist, c_jumpSpeed);

				}

				//alle Animationen und dafür benötigten werte werden ein gestellt

				if (gameObject.GetComponent<PlayerController> ().Char1) {
					animator.SetTrigger ("Tl_Jump");
					animator.SetBool ("Sl_Running", false);
					animator.SetBool ("Sl_Idle", false);

				} else {

					animator.SetTrigger ("Sh_Jump_start");
					animator.SetBool ("Sh_Running", false);
					animator.SetBool ("Sh_Idle", false);

				}
			}
			Invoke("unGround", 0.2f);
		}
	}
	void Animate()
	{
		
		if (grounded == true && groundedTrigger == false ) {
			
			if (gameObject.GetComponent<PlayerController> ().Char1) {
				animator.SetTrigger ("Tl_Jump_stop");
			} else {
				animator.SetTrigger ("Sh_Jump_stop");
			}
			groundedTrigger = true;
	
		}
	}
	void unGround(){
		groundedTrigger = false;
	
	}

}
