﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour {

	/// <summary>
	/// The bullet.
	/// </summary>

	public GameObject bulletPrefab;
	public GameObject bulletSpawn;

	public Animator animator;

	public bool flyingBullet = false;
	public float shootTimer;
	public float shootCoolDown = 0.1f;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (gameObject.GetComponent<PlayerController> ().Char2) 	//!GameObject.Find("Schalter").GetComponent<Schalter>().amSchalter zu griff auf das bool amSchalter im script Schalter auf dem schalter
		{
			if (Input.GetButtonDown ("X_Button") && !flyingBullet && !gameObject.GetComponent<PlayerController>().amSchalter  || Input.GetKeyDown (KeyCode.Y) && !gameObject.GetComponent<PlayerController>().amSchalter && !flyingBullet ) {
				flyingBullet = true;

				shootTimer = shootCoolDown;
				GameObject bullet = (GameObject)Instantiate (bulletPrefab, bulletSpawn.transform.position, Quaternion.identity);
				animator.SetTrigger ("Sh_Magic");

			}
			if (flyingBullet) {
				if (shootTimer > 0) {
					shootTimer -= Time.deltaTime;
				} else {
					flyingBullet = false;
					
				}




			}
		}

	
	}

}
