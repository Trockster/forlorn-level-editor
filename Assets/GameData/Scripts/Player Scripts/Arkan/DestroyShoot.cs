﻿using UnityEngine;
using System.Collections;

public class DestroyShoot : MonoBehaviour {

	public float lifetime;

		void Start () 
	{
		Destroy (gameObject, lifetime);


	}

	void OnTriggerEnter2D (Collider2D collide)
	{
		if(collide.gameObject.tag == "Wall" || collide.gameObject.tag == "Box" || collide.gameObject.tag == "Ground" || collide.gameObject.tag == "Fackel")
		{
			Destroy(gameObject);
		}

	}
}

