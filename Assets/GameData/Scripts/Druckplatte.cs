﻿using UnityEngine;
using System.Collections;

public class Druckplatte : MonoBehaviour {

	public Animator animator;
	public GameObject Door;


	void Start()
	{
		animator.SetTrigger ("Off");
	}
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D collide) {
		if (collide.gameObject.tag == "Box") {
			Door.SendMessage ("OpenGate");
			animator.SetTrigger ("On");
		}
	}
	void OnTriggerExit2D(Collider2D collide) {
		if (collide.gameObject.tag == "Box") {
			Door.SendMessage ("CloseGate");
			animator.SetTrigger ("Off");
		}
	}
}
