﻿using UnityEngine;
using System.Collections;

public class PlantSight : MonoBehaviour {

	public Animator animator;

	public GameObject PlantShotPrefab;
	public GameObject ShotSpawn;

	public float maxShootPressure = 1f;
	public float ShootPressure = 0;

	public bool left = true;

	public bool Agressive = false;

	// Use this for initialization
	void Start () 
	{
		E_Slime e_slime = GetComponent<E_Slime>();
	}
	
	// Update is called once per frame
	void Update () {
		
		ShotCharge ();
		if (Agressive) {
			shoot ();
		}
	}
	void ShotCharge ()
	{
		if (ShootPressure < maxShootPressure) 
		{
			ShootPressure += Time.deltaTime;
		} 
		else 
		{
			ShootPressure = maxShootPressure;
		}
	}
	void shoot()
	{
		if (ShootPressure >= maxShootPressure) {
			GameObject bullet = (GameObject)Instantiate (PlantShotPrefab, ShotSpawn.transform.position, Quaternion.identity);
			ShootPressure = 0;
				animator.SetTrigger ("Attack");
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player") 
		{
			AgressiveOn ();
		}
	}
	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			Invoke("AgressiveOff", 2f);
		}
	}
	public void AgressiveOn()
	{
		Agressive = true;
	}
	public void AgressiveOff()
	{
		Agressive = false;
		animator.SetBool ("Idle", true);
	}
}
