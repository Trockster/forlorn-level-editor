﻿using UnityEngine;
using System.Collections;

public class PlantBullet : MonoBehaviour {

	private GameObject myTarget;
	private Vector2 targetPos;

	public Rigidbody2D PlantShot;

	private bool plant;

	// Use this for initialization
	void Start () {
		plant = GameObject.Find ("PlantSight").GetComponent<PlantSight> ().left;

		myTarget = GameObject.FindGameObjectWithTag ("Player");
		targetPos = myTarget.transform.position;

		if (plant == true) 
			{
			Vector2 right = new Vector2 (-20, 0);
			PlantShot.AddForce (right, ForceMode2D.Impulse);
			transform.rotation = Quaternion.Euler (0, 180, 0);
			}
		if(plant == false)
			{
			Vector2 left = new Vector2 (20, 0 );
			PlantShot.AddForce (left, ForceMode2D.Impulse);
			}

	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		



	}
		






}
