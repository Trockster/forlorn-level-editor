﻿using UnityEngine;
using System.Collections;

public class PlantSightEX: MonoBehaviour {

	public Animator animator;

	public GameObject PlantShotPrefab;
	public GameObject ShotSpawn;

	public float maxTurnPressure = 5f;
	public float TurnPressure = 0;

	public float maxShootPressure = 1f;
	public float ShootPressure = 0;

	public bool left = true;


	public bool Agressive = false;


	// Use this for initialization
	void Start () 
	{
		E_Slime e_slime = GetComponent<E_Slime>();
	}
	
	// Update is called once per frame
	void Update () {
		Charge ();
		ShotCharge ();
		if (Agressive) {
			shoot ();
		}
	}
	void ShotCharge ()
	{
		if (ShootPressure < maxShootPressure) 
		{
			ShootPressure += Time.deltaTime;

		} 
		else 
		{
			ShootPressure = maxShootPressure;
		}
	}
	void Charge()
	{
		if (TurnPressure < maxTurnPressure) 
		{
			TurnPressure += Time.deltaTime;

		} 
		else 
		{
			TurnPressure = maxTurnPressure;
		}
		if (TurnPressure >= maxTurnPressure) 
		{
			TurnPressure = 0;
			if (!Agressive) {
				ChangeDir ();
			}
		}

	}
	void shoot()
	{
		if (ShootPressure >= maxShootPressure) {
			GameObject bullet = (GameObject)Instantiate (PlantShotPrefab, ShotSpawn.transform.position, Quaternion.identity);
			ShootPressure = 0;
			if (left) {
				animator.SetTrigger ("Attack");
			} else {
				animator.SetTrigger ("Attack2");
			}
		}
	}
	public void ChangeDir () {
		if (left) {
			left = false;
			transform.rotation = Quaternion.Euler (0, 180, 0);
			animator.SetBool ("Idle2", true);
			animator.SetBool ("Idle", false);
		} else {
			left = true;
			transform.rotation = Quaternion.Euler (0, 0, 0);
			animator.SetBool ("Idle", true);
			animator.SetBool ("Idle2", false);
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player") 
		{
			AgressiveOn ();
		}
	}
	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			Invoke("AgressiveOff", 2f);
		}
	}
	public void AgressiveOn()
	{
		Agressive = true;
	}
	public void AgressiveOff()
	{
		Agressive = false;

	}
}
