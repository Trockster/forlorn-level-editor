﻿using UnityEngine;
using System.Collections;

public class EnemyHitbox : MonoBehaviour {

	public Animator animator;

	[SerializeField]bool Hit_1;
	[SerializeField]bool Hit_2;
	[SerializeField]bool Hit_3;
	[SerializeField]bool Hit_4;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.name == "AttackTrigger") {
			HitByPlayer ();
		}
	}
	 void HitByPlayer()
	{
		animator.SetTrigger ("Hit");
		if (!Hit_1) {
			Hit_1 = true;
		} else {
			if (!Hit_2) {
				Hit_2 = true;
			} else {
				if (!Hit_3) {
					Hit_3 = true;
				} else {
					if (!Hit_4) {
						Hit_4 = true;
					} else {
						DestroyThis ();
					}
				}
			}
		}
	}
	void DestroyThis()
	{
		Destroy(transform.parent.parent.gameObject);
	}
		

}
