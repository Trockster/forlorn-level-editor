﻿using UnityEngine;
using System.Collections;

public class E_Slime: MonoBehaviour {

	public Animator animator;
	public bool Idle;
	private bool left = false;

	float jumpSpeed = 50;
	float jumpDistance = -25;

	float jumpPressure;
	float maxJumpPressure = 3f;

	public Rigidbody2D slime;

	public GameObject Target { get; set; }


	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask groundlayer;
	public bool grounded;

	// Use this for initialization
	void Start () {
		slime = GetComponent<Rigidbody2D> ();
		Idle = true;
	}

	void FixedUpdate () {
		//wie der ground gefunden wird und jedes boden stück brauch den layer ground
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, groundlayer);
	}

	// Update is called once per frame
	void Update () {
		Jump ();
		Charge ();	

		if (Idle) {
			animator.SetBool ("Idle", true);
		} else {
			animator.SetBool ("Attack", true);
		}
	}

	void Jump()
	{

		if (jumpPressure == maxJumpPressure)  
		{
			ChangeJumpDir ();
			slime.velocity = new Vector2 (jumpDistance, jumpSpeed );
			jumpPressure = 0;

		}
	}

	void Charge()
	{
		if (grounded) 
		{
			if (jumpPressure < maxJumpPressure) 
			{
				jumpPressure += Time.deltaTime;

			} 
			else 
			{
				jumpPressure = maxJumpPressure;
			}
		}
	}

	public void ChasePlayer()
	{
		//Debug.Log ("found");
		Idle = false;
		maxJumpPressure = 0.5f;
	}
	public void leavePlayer()
	{
		//Debug.Log ("lost");
		Idle = true;
		maxJumpPressure = 3f;
	}

	void ChangeJumpDir()
	{
		if (Idle)
		{
			if (left)
			{left = false;}
			else
			{left = true;}
		}
		
		if (left) {
			transform.rotation = Quaternion.Euler (0, 180, 0);
			jumpDistance = 25;
		} else {
			transform.rotation = Quaternion.Euler (0, 0, 0);
			jumpDistance = -25;
		}

	}
}
