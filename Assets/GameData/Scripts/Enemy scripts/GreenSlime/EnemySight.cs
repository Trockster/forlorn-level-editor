﻿using UnityEngine;
using System.Collections;

public class EnemySight : MonoBehaviour {

	[SerializeField]
	private E_Slime enemy;


	// Use this for initialization
	void Start () 
	{
		E_Slime e_slime = GetComponent<E_Slime>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player") 
		{
			enemy.ChasePlayer();
		}
	}
	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			enemy.leavePlayer();
		}
	}
}
