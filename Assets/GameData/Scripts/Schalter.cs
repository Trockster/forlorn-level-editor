﻿using UnityEngine;
using System.Collections;

public class Schalter : MonoBehaviour {

	public Animator animator;
	public GameObject Door;

	public bool Geschaltet = false;

	//public bool amSchalter = false;

	// Use this for initialization
	void LateUpdate () {
		if (Input.GetKeyDown (KeyCode.Y) && GameObject.Find("Player.2 3D").GetComponent<PlayerController>().amSchalter && GameObject.Find ("Player.2 3D").gameObject.GetComponent<PlayerController> ().Char1 || Input.GetButtonDown ("X_Button") && GameObject.Find("Player.2 3D").GetComponent<PlayerController>().amSchalter && GameObject.Find ("Player.2 3D").gameObject.GetComponent<PlayerController> ().Char1) {
			Debug.Log (2);
			if (Geschaltet == false) {
				Geschaltet = true;
				Door.SendMessage ("OpenGate");
				animator.SetTrigger ("On");
			} else {
				Geschaltet = false;
				Door.SendMessage ("CloseGate");
				animator.SetTrigger ("Off");
			}
			
		}
	}
/*
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D collide) {
		if (collide.gameObject.tag == "Player") {
			amSchalter = true;
			Debug.Log (1);

		}
	}
		void OnTriggerExit2D(Collider2D collide) {
			if (collide.gameObject.tag == "Player") {
				amSchalter = false;
				Debug.Log (3);
	}
}*/
}